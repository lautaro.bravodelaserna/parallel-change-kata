﻿using System.Collections.Generic;
using System.Linq;

namespace ParallelAndNarrowChange.Field
{
    public class ShoppingCart{
        private List<int> price;

        public ShoppingCart()
        {
            price = new List<int>();
        }

        public decimal CalculateTotalPrice(){
            return price.Sum(item => item);
        }

        public bool HasDiscount(){
            return price.Any(item => item > 100);
        }

        public void Add(int aPrice){
            price.Add(aPrice);
        }

        public int NumberOfProducts(){
            return price.Count;
        }
    }
}
